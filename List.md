
# LIST
- 2022-3-5:
  1. 增加最小子系统
  2. 增加ddr3 sdcard 仿真模型
  3. 优化目录结构
  4. 增加interconnect 
  5. axi 2 wb 转接有点问题 （弃用wishbone总线）

- 2022-3-26
  1. 重构SOC
  2. 增加NPU dummy
  3. 转为VCS仿真
  4. 地址重新映射
  5. CPU可替换
  
- 2022-4.12
  1. 增加E203 处理器核心
  2. 外设替换为E203 同等外设
  3. 软件环境分开rv32 与 rv64
  4. VCS 仿真载入的filelist 可以根据选择的CPU 引入不同的文件
  5. 新版UART跑通 rv64
  6. 增加BOOTROM自动生成流程 （vcs的makefile中  bin->sv）
  7. 修复RenaCoreV1 RenaCoreV2 中窄传输的严重BUG
  
- 2022-4.18
  1. 删除E203  
  2. 软件环境reset  
  3. 重新构建SOC架构 （不需要那么多层的总线桥）  
  4. 重构编译构建make流程 将配置集中与config.mk中 删除soc defines.v  
  5. 重构分配地址 

- 2022-4.20
  1. 增加cctp (cpu core test platfrom) 来支持cpu的差分测试验证
  2. 调通GPIO软硬件结构
  3. 优化NPU Debug 模块
  4. 增加分支预测 Dummy
  5. 注意！！！ VCS参数过长会导致仿真出现
```
*** buffer overflow detected ***: terminated
```
  6. 增加CCTP回归测试
  
- 2022-4.21
  1. 调了一上午BUG 结果是Difftest 的首地址映射错误 无语了
  2. 增加V3 版本

- 2022-4.22
  1. RenaCoreV3 优化IFU与BPU结构 逻辑更加高效 请求信号做了复位同步
  2. RenaCoreV3 重定向信息合并到提交阶段发送到前端、ifu携带bpu信息传到bru通路完成、bru携带真实分支信息传送到ifu通路完成
  3. RenaCoreV3 优化core.v文件中前端模块的例化方式
  4. RenaCoreV3 PHT优化为可配置深度 
  5. RenaCoreV3 不开启分支预测时 回归测试完毕 

- 2022-5.18
  1.测试Timer 

- 2022-8.22
  1. 分离处理器核心与SoC验证平台
  2. 8月份完成Rena升级计划

- 2022-8.28
  1. 使BPU正常工作
  2. 学习虚拟内存机制

- 2022-9.7
  1. Icache增加容量为16K 64*512*4 (cacheline 64B 4路组相连)
  2. 明晚处理双bank Icache
  3. 中秋完成 M拓展 和 重构Icache输出8指令

- 2022-9.8
  1. 双bank升级完成容量扩充为 32K = 64(cacheline) * 512(set) * 4(way) * 2(bank)
  2. Icache中TAG修改为SRAM实现
  3. 明天 升级IBUF为8入1出FIFO
 
-2022-9.9
  1. 完成IBUF4入4出升级
  2. 明天要重构Icache 考虑MMIO与MEM访问如何做

-2022-9.10 
  1. ICache输出为4*32bit

-2022-9.11
  1. 完成IBF4入4出设计，支持不定路有效输出
  2. 将Icache 访问非Cache内存时的逻辑放到BIU中进行
  3. Icache完善4指令输出，支持非4指令对齐输出（小于4条）
  4. bug: 1.Icache S3 与 S2 的Meta有效信号没有判断或者跨bank会造成错误旁路命中  -- 增加bank标识和优化控制信号
          2.4取指会使重定向刷新跨Cacheline -- IFU修改 PC重定向后一拍4指令对齐
          3.IBF会遇到4路不定有效信号不能很好的接收 -- 优化写入逻辑
          4.IBF满后总会输出一条错误指令  -- IBF满时，增加写入阻塞

-2022-9.12
  1. 接入简易MDU乘除法单元
  2. Dcache 完善修改为 4KB->64KB VIPT 4bank 4路组相联 cacheline 64B
  3. Dcache去掉MMIO检测 (放到BIU中处理) 优化主状态机设计 更新刷cache状态机
  5. 优化目录结构
  6. Icache 与 Dcache使用了两种bank的rtl写法 多个bank时还是Dcache的写法好用 （chisel更好用）
  6.bug: 1. Dcache SRAM写使能与写掩码使能搞反 -- bit取反 
         2. Icache 双bank没有使用一组V PLRU 会混淆命中 -- 增加多bank的meta信息

-2022-9.13 
  1. 修Dcache BUG

-2022-9.14
  1.增加gitee github双推送

- 国庆计划
  0. 完善riscv模拟器设计  delay
  1. 译码刷新机制（需要刷后面流水的指令，在译码刷新，节省功耗）。PASS
  2. 译码、执行、写回自维护PC , 减少多级PC传递浪费
  3. 两级分支预测机制 , 基础实现，不分析性能 delay
  4. 增加压缩指令集 PASS
  5. riscv-test 验证 I M C

-2022-10.5
  1.fix Icache
  2.add decoder compress instr

-2022-10.6
  1.增加压缩指令集              PASS
  2.译码刷新机制 完善流水线控制  PASS
  3.增加译码自维护PC机制        PASS    
  4.riscv-test移植到环境中
  5.debug

-2022-10.7
  1.MSU CSR 完善
  2.实现简易MMU
  3.debug VM

-2022-10/9
  1.修C BUG
  2.增加s CSR

-2022-10.29
  1.fix Dcache bank bug
  2.fix idu recv inst number logic
  3.fix C inst decoder logic