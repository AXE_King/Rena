#!/bin/bash

while getopts 'r:' OPT; do
    case $OPT in
        r) TEST_CASES="$OPTARG"; echo $OPTARG;;
    esac
done

mkdir log 1>/dev/null 2>&1

for FOLDER in ${TEST_CASES[@]}
do
    BIN_FILES=`eval "find $FOLDER -mindepth 1 -maxdepth 1 -regex \".*\.\(bin\)\""`
    for BIN_FILE in $BIN_FILES; do
        FILE_NAME=`basename ${BIN_FILE%.*}`
        printf "[%30s] " $FILE_NAME
        LOG_FILE=log/$FILE_NAME-log.txt
        ./difftest/simv +workload=$BIN_FILE &> $LOG_FILE
        # echo $BIN_FILE
        if (grep 'HIT GOOD TRAP' $LOG_FILE > /dev/null) then
            echo -e "[ PASS! ]"
            rm $LOG_FILE
        else
            echo -e "[ FAIL! ] see $LOG_FILE for more information"
        fi
    done
done

