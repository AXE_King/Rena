#ifndef __CONFIG_H
#define __CONFIG_H

#ifndef NUM_CORES
#define NUM_CORES 1
#endif

// -----------------------------------------------------------------------
// Memory and device config
// -----------------------------------------------------------------------

// emulated memory size (Byte)
// #define EMU_RAM_SIZE (256 * 1024 * 1024UL) // 256 MB
#define EMU_RAM_SIZE (4 * 1024 * 1024 * 1024UL) // 8 GB

// first valid instruction's address, difftest starts from this instruction
#define FIRST_INST_ADDRESS 0x80000000

// sdcard image to be used in simulation
// uncomment the follwing line to enable this feature
// #define SDCARD_IMAGE "/home/xyn/workloads/debian/riscv-debian.img"

// Use sdl to show screen
// Note: It does not work well with clang, to use that, switch to gcc
// uncomment the follwing line to enable this feature
// #define SHOW_SCREEN

// -----------------------------------------------------------------------
// Difftest interface config
// -----------------------------------------------------------------------

// max commit width
#define DIFFTEST_COMMIT_WIDTH 6

// max store width
#define DIFFTEST_STORE_WIDTH 2

// commit inst history length 
#define DEBUG_INST_TRACE_SIZE 32

// commit inst group history length 
#define DEBUG_GROUP_TRACE_SIZE 16

// -----------------------------------------------------------------------
// Checkpoint config
// -----------------------------------------------------------------------

// time to fork a new checkpoint process
#define FORK_INTERVAL 1 // unit: second

// max number of checkpoint process at a time 
#define SLOT_SIZE 2

// exit when error when fork 
#define FAIT_EXIT    exit(EXIT_FAILURE);

// process sleep time  
#define WAIT_INTERVAL 5

// time to save a snapshot
#define SNAPSHOT_INTERVAL 60 // unit: second

// -----------------------------------------------------------------------
// Memory difftest config
// -----------------------------------------------------------------------

// whether to check memory coherence during refilling
// #define DEBUG_REFILL

// dump all tilelink trace to a database
// uncomment the follwing line to enable this feature
//#define DEBUG_TILELINK

// -----------------------------------------------------------------------
// Do not touch
// -----------------------------------------------------------------------

// whether to maintain goldenmem
#if NUM_CORES>1
    #define DEBUG_GOLDENMEM
#endif

#ifdef DEBUG_REFILL
    #define DEBUG_GOLDENMEM
#endif

#endif