module DcacheData64x512bit(
  input                 CLK,
  input                 CEN,
  input                 WEN,
  input [511:0]         BWEN,
  input [5:0]           A,
  input [511:0]         D, 
  output reg [511:0]    Q
);
  parameter Bits = 512;
  parameter Word_Depth = 64;
  parameter Add_Width = 6;
  parameter Wen_Width = 512;
 
  wire cen  = ~CEN;
  wire wen  = ~WEN;
  wire [Wen_Width-1:0] bwen = BWEN;
 
  reg [Bits-1:0] ram [0:Word_Depth-1];
  always @(posedge CLK) begin
      if(cen && wen) begin
          ram[A] <= (D & bwen) | (ram[A] & ~bwen);
      end
      Q <= ram[A];
  end
wire [511:0] debug_37 = ram[37];
endmodule