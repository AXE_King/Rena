module IcachePtag64x19bit(
  input                 CLK , 
  input                 CEN , 
  input                 WEN , 
  input [18:0]          BWEN, 
  input [ 5:0]          A   , 
  input [18:0]          D   , 
  output reg [18:0]     Q     
);
  parameter Bits = 19;
  parameter Word_Depth = 64;
  parameter Wen_Width = 19;
 
  wire cen  = ~CEN;
  wire wen  = ~WEN;
  wire [Wen_Width-1:0] bwen = BWEN;
 
  reg [Bits-1:0] ram [0:Word_Depth-1];
  always @(posedge CLK) begin
      if(cen && wen) begin
          ram[A] <= (D & bwen) | (ram[A] & ~bwen);
      end
      Q <= ram[A];
  end

endmodule