module SRAM(
  input  clk,
  input  cs,
  input  wen,
  input  [127:0] mask,
  input  [5:0] addr,
  input  [127:0] din,
  output [127:0] dout
);

S011HD1P_X32Y2D128_BW u1(
  .CLK  (clk),
  .CEN  (~cs),
  .WEN  (~wen),
  .BWEN (mask),
  .A    (addr),
  .D    (din), 
  .Q    (dout)
);

endmodule

module S011HD1P_X32Y2D128_BW(
  input                 CLK,
  input                 CEN,
  input                 WEN,
  input [127:0]         BWEN,
  input [5:0]           A,
  input [127:0]         D, 
  output reg [127:0]    Q
);
  parameter Bits = 128;
  parameter Word_Depth = 64;
  parameter Add_Width = 6;
  parameter Wen_Width = 128;
 
  wire cen  = ~CEN;
  wire wen  = ~WEN;
  wire [Wen_Width-1:0] bwen = BWEN;
 
  reg [Bits-1:0] ram [0:Word_Depth-1];
  always @(posedge CLK) begin
      if(cen && wen) begin
          ram[A] <= (D & bwen) | (ram[A] & ~bwen);
      end
      Q <= ram[A];
  end

endmodule

module dpram #(parameter ADDR_WIDTH=6 ,DATA_WIDTH=8) (
  input [(DATA_WIDTH-1):0] data,
  input [(ADDR_WIDTH-1):0] read_addr,
  input [(ADDR_WIDTH-1):0] write_addr,
  input we,
  input clk,
  output [(DATA_WIDTH-1):0] q  
);
reg [DATA_WIDTH-1:0] ram[2**ADDR_WIDTH-1:0];
reg [(DATA_WIDTH-1):0] q_out;
always @ (posedge clk) begin
    if (we) begin
		ram[write_addr] <= data;
    end
	q_out <= ram[read_addr]; // read old data!
end
assign q = q_out ;
endmodule
