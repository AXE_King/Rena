`include "common_defines.v"
`define ADDR_W  32 // AXI 地址宽度
`define PADDR_W 56 // 物理地址宽度
`define VADDR_W 39 // 虚拟地址宽度
`define PAGE_W  12 // 4K页地址宽度
`define PTE_W   64 // 页表宽度
`define PTE_ARR 20 // 页表属性宽度

`define BRU_MSG_W `VADDR_W*2 + 4 + 2
`define BPU_PRE_W `VADDR_W + 1

`define BTB_DEPTH 7
`define BTB_TAG_W (`VADDR_W - `BTB_DEPTH - 1)
`define BTB_ENTRY_W (1 + `BTB_TAG_W + `VADDR_W - 1 + 2)

`define XLEN 64
`define ZERO 'd0

`define Y  1'b1
`define N  1'b0

`define FU_W    4
`define OP_W    4

`define SRC_XX  2'd0
`define SRC_RF  2'd1
`define SRC_OR  2'd2
`define SRC_IM  2'd2
`define SRC_PC  2'd2

`define FU_ALU  `FU_W'd1
`define FU_BRU  `FU_W'd2
`define FU_LSU  `FU_W'd3
`define FU_CSR  `FU_W'd4
`define FU_ALUW `FU_W'd5
`define FU_MDU  `FU_W'd6
`define FU_XXX  `FU_W'd0

`define ALU_ADD   `OP_W'd1
`define ALU_SUB   `OP_W'd2
`define ALU_SLL   `OP_W'd3
`define ALU_SRL   `OP_W'd4
`define ALU_SRA   `OP_W'd5
`define ALU_AND   `OP_W'd6
`define ALU_OR    `OP_W'd7
`define ALU_XOR   `OP_W'd8
`define ALU_SLT   `OP_W'd9
`define ALU_SLTU  `OP_W'd10
`define ALU_XXXX  `OP_W'd0

`define MDU_MUL   `OP_W'b0000
`define MDU_MULH  `OP_W'b0001
`define MDU_MULHSU`OP_W'b0010
`define MDU_MULHU `OP_W'b0011
`define MDU_DIV   `OP_W'b0100
`define MDU_DIVU  `OP_W'b0101
`define MDU_REM   `OP_W'b0110
`define MDU_REMU  `OP_W'b0111
`define MDU_MULW  `OP_W'b1000
`define MDU_DIVW  `OP_W'b1100
`define MDU_DIVUW `OP_W'b1101
`define MDU_REMW  `OP_W'b1110
`define MDU_REMUW `OP_W'b1111

`define BRU_JAL   `OP_W'h1
`define BRU_JALR  `OP_W'h2
`define BRU_BEQ   `OP_W'h3
`define BRU_BNE   `OP_W'h4
`define BRU_BGE   `OP_W'h5
`define BRU_BGEU  `OP_W'h6
`define BRU_BLT   `OP_W'h7
`define BRU_BLTU  `OP_W'h8
`define BRU_CJAL  `OP_W'h9
`define BRU_CJALR `OP_W'hA
`define BRU_X     `OP_W'h0

// Memory Mask Type Signal 
// LDU is illegal
`define LSU_RBS  `OP_W'b0000
`define LSU_RHS  `OP_W'b0010
`define LSU_RWS  `OP_W'b0100
`define LSU_RDX  `OP_W'b0110 
`define LSU_RBU  `OP_W'b0001
`define LSU_RHU  `OP_W'b0011
`define LSU_RWU  `OP_W'b0101
`define LSU_WBX  `OP_W'b1000
`define LSU_WHX  `OP_W'b1010
`define LSU_WWX  `OP_W'b1100
`define LSU_WDX  `OP_W'b1110 
`define LSU_XXX  `OP_W'b1111
`define LSU_FEI  `OP_W'b0111
// CSR 
`define CSR_W  `OP_W'd1 // W   bits
`define CSR_S  `OP_W'd2 // Set bits
`define CSR_C  `OP_W'd3 // Clr bits
`define CSR_P  `OP_W'd4 // 特权级操作
`define CSR_X  `OP_W'd0 // 非CSR操作

// 立即数类型
`define IMM_X   3'd0
`define IMM_I   3'd1 // CUL 
`define IMM_S   3'd2 // store
`define IMM_B   3'd3 // b
`define IMM_U   3'd4 // u
`define IMM_J   3'd5 // jal
`define IMM_R   3'd7 // csr

// 特权级
`define PRV_U  2'd0
`define PRV_S  2'd1
`define PRV_H  2'd2
`define PRV_M  2'd3

`define EXCEPT 16
`define ecall   0
`define mret    1
`define sret    2
`define uret    3
`define ebreak  4
`define clint   5
`define ilginst 6
`define fencei  7
