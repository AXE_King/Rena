`include "defines.v"
module instrScan(
  input  [31:0] instr,
  output insrtScan_ras_Call_o,
  output insrtScan_ras_Ret_o
);
// Call jalr x1  xx(xx)
assign insrtScan_ras_Call_o =1'b0& (instr[6:0] == 7'b1100111) 
                            && (instr[11:7] == 5'd1) 
                            && (instr[14:12] == 3'b010);
// Ret  jalr x0, 0(x1)
assign insrtScan_ras_Ret_o  = 1'b0&(instr[6:0] == 7'b1100111) 
                            && (instr[11:7] == 5'd0) 
                            && (instr[14:12] == 3'b010)
                            && (instr[19:15] == 5'd1)
                            && (instr[31:20] == 'd0);
// 跳转指令
// assign insrtScan_btb_brunch_o = (instr[6:0] == 7'b1100011) || (instr[6:0] == 7'b1100111) ;
endmodule