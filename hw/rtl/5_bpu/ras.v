`include "defines.v"
module ras(
  input clk  ,
  input rst_n,
  input  valid,
  input  isCall,
  input  isRet ,
  input  [`VADDR_W-1:0] gen_ras_pc_i,
  output taken_o,
  output [`VADDR_W-1:0] ras_gen_jump_pc_o
);
  reg [31:0] ras_stack [0:7]; //1bit valid 7bit cnt 32bit retaddr

  reg [2:0] ptr;
  always@(posedge clk or negedge rst_n)
    if(~rst_n)
      ptr <= 3'd0;
    else if (isCall && valid) begin
      ptr <= ptr + 1'b1;
    end else if(isRet && valid) begin
      ptr <= ptr - 1'b1;
    end
  
  assign taken_o = isRet && valid;
  assign ras_gen_jump_pc_o = ras_stack[ptr][31:0];

  genvar i;
  generate 
    for(i=0;i<8;i=i+1)begin
      always@(posedge clk or negedge rst_n)
        if(~rst_n)
          ras_stack[i] <= 32'd0;
        else if(isCall && valid && (ptr == i))
          ras_stack[i] <= gen_ras_pc_i + 32'd4;
    end 
  endgenerate

endmodule