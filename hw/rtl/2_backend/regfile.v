`include "defines.v"
module regfile(
  input  clk  ,
  input  rst_n,
  input  wen,
  input  [4:0]waddr,
  input  [`XLEN-1:0] wdata,
  input  ren1,
  input  [4:0]raddr1,
  output [`XLEN-1:0] rdata1,
  input  ren2,
  input  [4:0]raddr2,
  output [`XLEN-1:0]rdata2
`ifdef DIFFTEST
  ,output [7:0] debug_a0
`endif
);

reg [`XLEN-1:0] int_rf [0:31];

genvar i;
generate
  for(i=0;i<32;i=i+1)begin
    always @(posedge clk) begin
      if(~rst_n)      
        int_rf[i] <= `ZERO;
      else if (wen && waddr != 'd0 && waddr == i)
        int_rf[i] <= wdata;
    end
  end
endgenerate

assign rdata1 = ren1 ? int_rf[raddr1] : 'd0;
assign rdata2 = ren2 ? int_rf[raddr2] : 'd0;

`ifdef DIFFTEST
assign debug_a0 = int_rf[10][7:0];

DifftestArchIntRegState DifftestArchIntRegState (
  .clock              (clk),
  .coreid             (0),
  .gpr_0              (int_rf[0]),
  .gpr_1              (int_rf[1]),
  .gpr_2              (int_rf[2]),
  .gpr_3              (int_rf[3]),
  .gpr_4              (int_rf[4]),
  .gpr_5              (int_rf[5]),
  .gpr_6              (int_rf[6]),
  .gpr_7              (int_rf[7]),
  .gpr_8              (int_rf[8]),
  .gpr_9              (int_rf[9]),
  .gpr_10             (int_rf[10]),
  .gpr_11             (int_rf[11]),
  .gpr_12             (int_rf[12]),
  .gpr_13             (int_rf[13]),
  .gpr_14             (int_rf[14]),
  .gpr_15             (int_rf[15]),
  .gpr_16             (int_rf[16]),
  .gpr_17             (int_rf[17]),
  .gpr_18             (int_rf[18]),
  .gpr_19             (int_rf[19]),
  .gpr_20             (int_rf[20]),
  .gpr_21             (int_rf[21]),
  .gpr_22             (int_rf[22]),
  .gpr_23             (int_rf[23]),
  .gpr_24             (int_rf[24]),
  .gpr_25             (int_rf[25]),
  .gpr_26             (int_rf[26]),
  .gpr_27             (int_rf[27]),
  .gpr_28             (int_rf[28]),
  .gpr_29             (int_rf[29]),
  .gpr_30             (int_rf[30]),
  .gpr_31             (int_rf[31])
);
`endif
endmodule