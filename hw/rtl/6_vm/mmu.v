`include "defines.v"
module mmu(
  input clk,
  input rst_n,
  // system message
  
  // I MMU 
  input                  ifu_mmu_trans_valid_i,
  output                 mmu_ifu_trans_ready_o,
  input  [`VADDR_W-1:0]  ifu_mmu_trans_vaddr_i,
  output [`PADDR_W-1:0]  mmu_ifu_trans_paddr_o,
  // D MMU
  input                  exu_mmu_trans_valid_i,
  output                 mmu_exu_trans_ready_o,
  input  [`VADDR_W-1:0]  exu_mmu_trans_vaddr_i,
  output [`PADDR_W-1:0]  mmu_exu_trans_paddr_o
  // Dcache Access

);
// 不开启分页
assign mmu_ifu_trans_ready_o = `Y;
assign mmu_ifu_trans_paddr_o = ifu_mmu_trans_vaddr_i;
assign mmu_exu_trans_ready_o = `Y;
assign mmu_exu_trans_paddr_o = exu_mmu_trans_vaddr_i;

endmodule