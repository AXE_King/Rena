`include "defines.v"
module reqDispute(
  // user request
  input  dcache_mem_addr_valid_i,
  input  dcache_mem_wen_i,
  input  [7:0]dcache_mem_strb_i,
  input  [`ADDR_W-1:0] dcache_mem_addr_i,
  input  [511:0] dcache_mem_wdata_i,
  output mem_dcache_data_valid_o,
  output [511:0] mem_dcache_rdata_o,
  // read request
  output  rd_addr_valid_o,
  output  [`ADDR_W-1:0]rd_addr_o,
  input   rd_data_valid_i,
  input   [511:0]rd_data_i,
  // write request
  output   wr_addr_valid_o,
  output   [`ADDR_W-1:0]wr_addr_o,
  output   [7:0] wr_strb_o,
  output   [127:0]wr_data_o,
  input    wr_data_valid_i
);
wire wen = dcache_mem_wen_i ;
// 请求下发通道
assign rd_addr_valid_o  = ~wen && dcache_mem_addr_valid_i;  
assign wr_addr_valid_o  =  wen && dcache_mem_addr_valid_i;
assign rd_addr_o  = dcache_mem_addr_i;    
assign wr_addr_o  = dcache_mem_addr_i;  
assign wr_strb_o  = dcache_mem_strb_i;
assign wr_data_o  = dcache_mem_wdata_i;
// 回复上载通道
assign mem_dcache_data_valid_o = wen ? wr_data_valid_i : rd_data_valid_i;  
assign mem_dcache_rdata_o      = rd_data_i; 
endmodule