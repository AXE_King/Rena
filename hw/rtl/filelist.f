+incdir+$CPU_HOME
+incdir+$CPU_HOME/0_common

$CPU_HOME/0_common/IcachePtag64x19bit.v
$CPU_HOME/0_common/IcacheData64x512bit.v
$CPU_HOME/0_common/DcachePtag64x18bit.v
$CPU_HOME/0_common/DcacheData64x512bit.v
$CPU_HOME/0_common/SRAM.v

$CPU_HOME/0_common/common_defines.v
$CPU_HOME/0_common/inst.v
$CPU_HOME/0_common/defines.v

$CPU_HOME/1_frontend/ifu.v
$CPU_HOME/1_frontend/Icache.v
$CPU_HOME/1_frontend/ibf.v

$CPU_HOME/2_backend/idu.v
$CPU_HOME/2_backend/exu.v
$CPU_HOME/2_backend/regfile.v
$CPU_HOME/2_backend/wbu.v
$CPU_HOME/2_backend/isu.v
$CPU_HOME/2_backend/fu/clint.v
$CPU_HOME/2_backend/fu/lsu.v
$CPU_HOME/2_backend/fu/mdu.v
$CPU_HOME/2_backend/fu/alu.v
$CPU_HOME/2_backend/fu/Dcache.v
$CPU_HOME/2_backend/fu/csrWriteBuffer.v
$CPU_HOME/2_backend/fu/bru.v

$CPU_HOME/3_biu/biu.v
$CPU_HOME/3_biu/rdAXI.v
$CPU_HOME/3_biu/wrAXI.v
$CPU_HOME/3_biu/rdArbiter.v
$CPU_HOME/3_biu/reqDispute.v

$CPU_HOME/4_ctrl/pipelineControl.v
$CPU_HOME/4_ctrl/csr.v

$CPU_HOME/5_bpu/bpu.v
$CPU_HOME/5_bpu/ras.v
$CPU_HOME/5_bpu/instrScan.v
$CPU_HOME/5_bpu/bht.v
$CPU_HOME/5_bpu/btb.v

$CPU_HOME/6_vm/mmu.v


$CPU_HOME/Rena.v
