`include "defines.v"
module pipelineControl(
  input rst_n,
  // flush request
  input wbu_redict_valid_i,
  // stall request
  input idu_stall_i,
  // stall signal 
  output ctrl_ifu_stall_o,
  output ctrl_icache_stall_o,
  output ctrl_ibf_stall_o,
  output ctrl_idu_stall_o,
  output ctrl_isu_stall_o,
  output ctrl_exu_stall_o,
  output ctrl_wbu_stall_o,
  // flush signal
  output ctrl_icache_flush_o,
  output ctrl_ibf_flush_o,
  output ctrl_idu_flush_o,
  output ctrl_isu_flush_o,
  output ctrl_exu_flush_o,
  output ctrl_wbu_flush_o
);
assign ctrl_ifu_stall_o    = idu_stall_i;
assign ctrl_icache_stall_o = idu_stall_i;
assign ctrl_ibf_stall_o    = idu_stall_i;
assign ctrl_idu_stall_o    = idu_stall_i;
assign ctrl_isu_stall_o    = 'd0;
assign ctrl_exu_stall_o    = 'd0;
assign ctrl_wbu_stall_o    = 'd0;

assign ctrl_icache_flush_o = wbu_redict_valid_i;
assign ctrl_ibf_flush_o    = wbu_redict_valid_i;
assign ctrl_idu_flush_o    = wbu_redict_valid_i;
assign ctrl_isu_flush_o    = wbu_redict_valid_i;
assign ctrl_exu_flush_o    = wbu_redict_valid_i;
assign ctrl_wbu_flush_o    = wbu_redict_valid_i;

endmodule