`include "defines.v"
module ibf#(
  parameter DataW = 32,
  parameter Depth = 32
)(
  input clk,
  input rst_n,

  input flush,
  // input stall,

  input  in_valid,
  output in_ready,
  // output almost_full,
  input [DataW-1:0] din,

  output out_valid,
  input  out_ready,
  output [DataW-1:0] dout
);

  reg [DataW-1:0] fifo_reg [0:Depth-1];
  reg [$clog2(Depth):0] wr_ptr;
  reg [$clog2(Depth):0] rd_ptr;
  wire full,empty;
  wire wen,ren;

  always@(posedge clk or negedge rst_n)
    if(~rst_n)
      wr_ptr <= 'd0;
    else if(flush)
      wr_ptr <= 'd0;
    else if((wen && ~full) || (wen && full && ren))
      wr_ptr <= wr_ptr + 1'b1;

  always@(posedge clk or negedge rst_n)
    if(~rst_n)
      rd_ptr <= 'd0;
    else if(flush)
      rd_ptr <= 'd0;
    else if((ren && ~empty) || (ren && wen && empty))
      rd_ptr <= rd_ptr + 1'b1;

  always@(posedge clk )
    if(wen && ~full)
      fifo_reg[wr_ptr[$clog2(Depth)-1:0]] <= din;
    else if(wen && full && ren)
      fifo_reg[wr_ptr[$clog2(Depth)-1:0]] <= din;
  
  assign dout = (wen && ren && empty) ? din : fifo_reg[rd_ptr[$clog2(Depth)-1:0]];

  assign full  = ( rd_ptr[$clog2(Depth)-1:0] == wr_ptr[$clog2(Depth)-1:0] ) && (rd_ptr[$clog2(Depth)] != wr_ptr[$clog2(Depth)]);
  // assign almost_full = ( rd_ptr[$clog2(Depth)-1:0] == (wr_ptr[$clog2(Depth)-1:0] + 1'b1) ) && (rd_ptr[$clog2(Depth)] != wr_ptr[$clog2(Depth)]);
  assign empty = rd_ptr == wr_ptr;
  assign wen   = in_valid;
  assign ren   = out_ready;
  assign in_ready  = (~full || (wen && full && ren));
  assign out_valid = ~empty || (ren && wen && empty);

endmodule

module IBF4In4Out #(
  parameter ENTRY_WIDTH = 32,
  parameter DEPTH       = 16,
  parameter ADDR_WIDTH  = $clog2(DEPTH)
)(
  input  clk,
  input  rst_n,
  input  flush,
  input  stall,

  output notfull    , 
  input  In0Valid,
  input  In1Valid,
  input  In2Valid,
  input  In3Valid,

  input [ENTRY_WIDTH-1:0] In0Entry,
  input [ENTRY_WIDTH-1:0] In1Entry,
  input [ENTRY_WIDTH-1:0] In2Entry,
  input [ENTRY_WIDTH-1:0] In3Entry,

  input [2:0] useInstNum,
  output Out0Valid,
  output Out1Valid,
  output Out2Valid,
  output Out3Valid,

  output [ENTRY_WIDTH-1:0] Out0Entry,
  output [ENTRY_WIDTH-1:0] Out1Entry,
  output [ENTRY_WIDTH-1:0] Out2Entry,
  output [ENTRY_WIDTH-1:0] Out3Entry
);

reg [ENTRY_WIDTH-1:0] QueueMem [0:DEPTH-1];

reg [ADDR_WIDTH:0] headPt; 
reg [ADDR_WIDTH:0] tailPt; 
reg [ADDR_WIDTH:0] nextHeadPt; 
reg [ADDR_WIDTH:0] nextTailPt; 

wire [ADDR_WIDTH:0] MemUseNum = flush ? 'd0 : ((headPt[ADDR_WIDTH] == tailPt[ADDR_WIDTH]) ? (tailPt[ADDR_WIDTH-1:0] - headPt[ADDR_WIDTH-1:0]) 
                                  : (DEPTH +tailPt[ADDR_WIDTH-1:0] - headPt[ADDR_WIDTH-1:0]));

always@(*)begin
  if(flush)
    nextTailPt = headPt;
  else if(notfull)
    nextTailPt = tailPt + In0Valid + In1Valid + In2Valid + In3Valid;
  else 
    nextTailPt = tailPt;

  if (MemUseNum != 'd0)
    nextHeadPt = headPt + useInstNum;
  else
    nextHeadPt = headPt;
end

always@(posedge clk or negedge rst_n)
  if(~rst_n)
    headPt <= 'd0;
  else 
    headPt <= nextHeadPt;

always@(posedge clk or negedge rst_n)
  if(~rst_n)
    tailPt <= 'd0;
  else 
    tailPt <= nextTailPt;

wire full = ((headPt[ADDR_WIDTH] != tailPt[ADDR_WIDTH]) && ((headPt[ADDR_WIDTH-1:0] - tailPt[ADDR_WIDTH-1:0]) < 'd4)) || 
              ((headPt[ADDR_WIDTH] == tailPt[ADDR_WIDTH]) && ((DEPTH + headPt[ADDR_WIDTH-1:0] - tailPt[ADDR_WIDTH-1:0]) < 'd4));
assign notfull = ~full ;

wire [ADDR_WIDTH-1:0] headPtAdd0 = headPt[ADDR_WIDTH-1:0] + 'd0;
wire [ADDR_WIDTH-1:0] headPtAdd1 = headPt[ADDR_WIDTH-1:0] + 'd1;
wire [ADDR_WIDTH-1:0] headPtAdd2 = headPt[ADDR_WIDTH-1:0] + 'd2;
wire [ADDR_WIDTH-1:0] headPtAdd3 = headPt[ADDR_WIDTH-1:0] + 'd3;

wire [ADDR_WIDTH-1:0] tailPtAdd0 = tailPt[ADDR_WIDTH-1:0] + 'd0;
wire [ADDR_WIDTH-1:0] tailPtAdd1 = tailPt[ADDR_WIDTH-1:0] + 'd1;
wire [ADDR_WIDTH-1:0] tailPtAdd2 = tailPt[ADDR_WIDTH-1:0] + 'd2;
wire [ADDR_WIDTH-1:0] tailPtAdd3 = tailPt[ADDR_WIDTH-1:0] + 'd3;
wire [3:0] validArray = {In3Valid,In2Valid,In1Valid,In0Valid};
genvar i;
generate
  for(i=0;i<DEPTH;i=i+1)begin : Q
    always@(posedge clk or negedge rst_n)
      if(~rst_n) QueueMem[i] <= 'd0;
      else if (~full & (tailPtAdd0 == i)) QueueMem[i] <= validArray[0] ? In0Entry : 
                                               validArray[1] ? In1Entry :
                                               validArray[2] ? In2Entry :
                                               validArray[3] ? In3Entry : QueueMem[i];

      else if (~full & (tailPtAdd1 == i)) QueueMem[i] <= (validArray[1:0] == 2'b11) ? In1Entry :
          ((validArray[2:0] == 3'b101)  || (validArray[2:0] == 3'b110))    ? In2Entry :
          ((validArray[3:0] == 4'b1100) || (validArray[3:0] == 4'b1010) || (validArray[3:0] == 4'b1001)) ? In3Entry : QueueMem[i];

      else if (~full & (tailPtAdd2 == i)) QueueMem[i] <= (validArray[2:0] == 3'b111) ? In2Entry :
            ((validArray[3:0] == 4'b1101) || (validArray[3:0] == 4'b1110) || (validArray[3:0] == 4'b1011)) ? In3Entry : QueueMem[i];

      else if (~full & (tailPtAdd3 == i)) QueueMem[i] <= (validArray[3:0] == 4'b1111) ? In3Entry : QueueMem[i];
  end
endgenerate
 
assign Out0Entry = QueueMem[headPtAdd0];
assign Out1Entry = QueueMem[headPtAdd1];
assign Out2Entry = QueueMem[headPtAdd2];
assign Out3Entry = QueueMem[headPtAdd3];

assign Out0Valid = MemUseNum >= 'd1;
assign Out1Valid = MemUseNum >= 'd2;
assign Out2Valid = MemUseNum >= 'd3;
assign Out3Valid = MemUseNum >= 'd4;

endmodule

module IBF #(
  parameter DEPTH = 32
)(
  input        clk,
  input        rst_n,
  input        flush,

  input [7:0]  icache_ibf_valid_i   ,
  output       ibf_icache_ready_o   ,
  input [15:0] icache_ibf_instIn0_i ,
  input [15:0] icache_ibf_instIn1_i ,
  input [15:0] icache_ibf_instIn2_i ,
  input [15:0] icache_ibf_instIn3_i ,
  input [15:0] icache_ibf_instIn4_i ,
  input [15:0] icache_ibf_instIn5_i ,
  input [15:0] icache_ibf_instIn6_i ,
  input [15:0] icache_ibf_instIn7_i ,

  output [7:0] ibf_idu_valid_o,
  input  [2:0] idu_ibf_ready_i,
  output [15:0] ibf_idu_instIn0_o ,
  output [15:0] ibf_idu_instIn1_o ,
  output [15:0] ibf_idu_instIn2_o ,
  output [15:0] ibf_idu_instIn3_o ,
  output [15:0] ibf_idu_instIn4_o ,
  output [15:0] ibf_idu_instIn5_o ,
  output [15:0] ibf_idu_instIn6_o ,
  output [15:0] ibf_idu_instIn7_o 
);
parameter ADDR_WIDTH  = $clog2(DEPTH);

reg [15:0] InstrQueue [0:DEPTH-1];

reg [ADDR_WIDTH:0] headPt; 
reg [ADDR_WIDTH:0] tailPt; 
reg [ADDR_WIDTH:0] nextHeadPt; 
reg [ADDR_WIDTH:0] nextTailPt; 
wire notfull;

wire [ADDR_WIDTH:0] MemUseNum  = flush ? 'd0 : ((headPt[ADDR_WIDTH] == tailPt[ADDR_WIDTH]) ? (tailPt[ADDR_WIDTH-1:0] - headPt[ADDR_WIDTH-1:0]) 
                                  : (DEPTH +tailPt[ADDR_WIDTH-1:0] - headPt[ADDR_WIDTH-1:0]));
wire [ADDR_WIDTH:0] useInstNum = idu_ibf_ready_i[2:0]; 

always@(*)begin
  if(flush)
    nextTailPt = headPt;
  else if(notfull)
    nextTailPt = tailPt + icache_ibf_valid_i[0] +
                          icache_ibf_valid_i[1] +
                          icache_ibf_valid_i[2] +
                          icache_ibf_valid_i[3] +
                          icache_ibf_valid_i[4] +
                          icache_ibf_valid_i[5] +
                          icache_ibf_valid_i[6] +
                          icache_ibf_valid_i[7] ;
  else 
    nextTailPt = tailPt;

  if (MemUseNum != 'd0)
    nextHeadPt = headPt + useInstNum;
  else
    nextHeadPt = headPt;
end

always@(posedge clk or negedge rst_n)
  if(~rst_n)
    headPt <= 'd0;
  else 
    headPt <= nextHeadPt;

always@(posedge clk or negedge rst_n)
  if(~rst_n)
    tailPt <= 'd0;
  else 
    tailPt <= nextTailPt;

wire full = ((headPt[ADDR_WIDTH] != tailPt[ADDR_WIDTH]) && ((headPt[ADDR_WIDTH-1:0] - tailPt[ADDR_WIDTH-1:0]) < 'd8)) || 
              ((headPt[ADDR_WIDTH] == tailPt[ADDR_WIDTH]) && ((DEPTH + headPt[ADDR_WIDTH-1:0] - tailPt[ADDR_WIDTH-1:0]) < 'd8));

assign notfull = ~full ;
assign ibf_icache_ready_o = notfull ;

wire [ADDR_WIDTH-1:0] headPtAdd0 = headPt[ADDR_WIDTH-1:0] + 'd0;
wire [ADDR_WIDTH-1:0] headPtAdd1 = headPt[ADDR_WIDTH-1:0] + 'd1;
wire [ADDR_WIDTH-1:0] headPtAdd2 = headPt[ADDR_WIDTH-1:0] + 'd2;
wire [ADDR_WIDTH-1:0] headPtAdd3 = headPt[ADDR_WIDTH-1:0] + 'd3;
wire [ADDR_WIDTH-1:0] headPtAdd4 = headPt[ADDR_WIDTH-1:0] + 'd4;
wire [ADDR_WIDTH-1:0] headPtAdd5 = headPt[ADDR_WIDTH-1:0] + 'd5;
wire [ADDR_WIDTH-1:0] headPtAdd6 = headPt[ADDR_WIDTH-1:0] + 'd6;
wire [ADDR_WIDTH-1:0] headPtAdd7 = headPt[ADDR_WIDTH-1:0] + 'd7;

wire [ADDR_WIDTH-1:0] tailPtAdd0 = tailPt[ADDR_WIDTH-1:0] + 'd0;
wire [ADDR_WIDTH-1:0] tailPtAdd1 = tailPt[ADDR_WIDTH-1:0] + 'd1;
wire [ADDR_WIDTH-1:0] tailPtAdd2 = tailPt[ADDR_WIDTH-1:0] + 'd2;
wire [ADDR_WIDTH-1:0] tailPtAdd3 = tailPt[ADDR_WIDTH-1:0] + 'd3;
wire [ADDR_WIDTH-1:0] tailPtAdd4 = tailPt[ADDR_WIDTH-1:0] + 'd4;
wire [ADDR_WIDTH-1:0] tailPtAdd5 = tailPt[ADDR_WIDTH-1:0] + 'd5;
wire [ADDR_WIDTH-1:0] tailPtAdd6 = tailPt[ADDR_WIDTH-1:0] + 'd6;
wire [ADDR_WIDTH-1:0] tailPtAdd7 = tailPt[ADDR_WIDTH-1:0] + 'd7;

wire [15:0] input2DM_IN  [0:7];
reg  [15:0] input2DM_OUT [0:7];

assign input2DM_IN[0] = icache_ibf_instIn0_i;
assign input2DM_IN[1] = icache_ibf_instIn1_i;
assign input2DM_IN[2] = icache_ibf_instIn2_i;
assign input2DM_IN[3] = icache_ibf_instIn3_i;
assign input2DM_IN[4] = icache_ibf_instIn4_i;
assign input2DM_IN[5] = icache_ibf_instIn5_i;
assign input2DM_IN[6] = icache_ibf_instIn6_i;
assign input2DM_IN[7] = icache_ibf_instIn7_i;

always@(*)begin
  if(icache_ibf_valid_i[0])begin
    input2DM_OUT[0] = input2DM_IN[0]; 
    input2DM_OUT[1] = input2DM_IN[1]; 
    input2DM_OUT[2] = input2DM_IN[2]; 
    input2DM_OUT[3] = input2DM_IN[3]; 
    input2DM_OUT[4] = input2DM_IN[4]; 
    input2DM_OUT[5] = input2DM_IN[5]; 
    input2DM_OUT[6] = input2DM_IN[6]; 
    input2DM_OUT[7] = input2DM_IN[7]; 
  end else if(icache_ibf_valid_i[1])begin
    input2DM_OUT[0] = input2DM_IN[1]; 
    input2DM_OUT[1] = input2DM_IN[2]; 
    input2DM_OUT[2] = input2DM_IN[3]; 
    input2DM_OUT[3] = input2DM_IN[4]; 
    input2DM_OUT[4] = input2DM_IN[5]; 
    input2DM_OUT[5] = input2DM_IN[6]; 
    input2DM_OUT[6] = input2DM_IN[7]; 
    input2DM_OUT[7] = 'd0;
  end else if(icache_ibf_valid_i[2])begin
    input2DM_OUT[0] = input2DM_IN[2]; 
    input2DM_OUT[1] = input2DM_IN[3]; 
    input2DM_OUT[2] = input2DM_IN[4]; 
    input2DM_OUT[3] = input2DM_IN[5]; 
    input2DM_OUT[4] = input2DM_IN[6]; 
    input2DM_OUT[5] = input2DM_IN[7]; 
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;    
  end else if(icache_ibf_valid_i[3])begin
    input2DM_OUT[0] = input2DM_IN[3]; 
    input2DM_OUT[1] = input2DM_IN[4]; 
    input2DM_OUT[2] = input2DM_IN[5]; 
    input2DM_OUT[3] = input2DM_IN[6]; 
    input2DM_OUT[4] = input2DM_IN[7]; 
    input2DM_OUT[5] = 'd0;
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;  
  end else if(icache_ibf_valid_i[4])begin
    input2DM_OUT[0] = input2DM_IN[4]; 
    input2DM_OUT[1] = input2DM_IN[5]; 
    input2DM_OUT[2] = input2DM_IN[6]; 
    input2DM_OUT[3] = input2DM_IN[7]; 
    input2DM_OUT[4] = 'd0; 
    input2DM_OUT[5] = 'd0;
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;
  end else if(icache_ibf_valid_i[5])begin
    input2DM_OUT[0] = input2DM_IN[5]; 
    input2DM_OUT[1] = input2DM_IN[6]; 
    input2DM_OUT[2] = input2DM_IN[7]; 
    input2DM_OUT[3] = 'd0; 
    input2DM_OUT[4] = 'd0; 
    input2DM_OUT[5] = 'd0;
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;
  end else if(icache_ibf_valid_i[6])begin
    input2DM_OUT[0] = input2DM_IN[6]; 
    input2DM_OUT[1] = input2DM_IN[7]; 
    input2DM_OUT[2] = 'd0; 
    input2DM_OUT[3] = 'd0; 
    input2DM_OUT[4] = 'd0; 
    input2DM_OUT[5] = 'd0;
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;
  end else if(icache_ibf_valid_i[7])begin
    input2DM_OUT[0] = input2DM_IN[7]; 
    input2DM_OUT[1] = 'd0; 
    input2DM_OUT[2] = 'd0; 
    input2DM_OUT[3] = 'd0; 
    input2DM_OUT[4] = 'd0; 
    input2DM_OUT[5] = 'd0;
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;
  end else begin
    input2DM_OUT[0] = 'd0; 
    input2DM_OUT[1] = 'd0; 
    input2DM_OUT[2] = 'd0; 
    input2DM_OUT[3] = 'd0; 
    input2DM_OUT[4] = 'd0; 
    input2DM_OUT[5] = 'd0;
    input2DM_OUT[6] = 'd0; 
    input2DM_OUT[7] = 'd0;
  end
end

reg [7:0] inValid;
always@(*)begin
  
  if     (icache_ibf_valid_i[0])inValid = 8'b1111_1111;
  else if(icache_ibf_valid_i[1])inValid = 8'b0111_1111;
  else if(icache_ibf_valid_i[2])inValid = 8'b0011_1111;
  else if(icache_ibf_valid_i[3])inValid = 8'b0001_1111;
  else if(icache_ibf_valid_i[4])inValid = 8'b0000_1111;
  else if(icache_ibf_valid_i[5])inValid = 8'b0000_0111;
  else if(icache_ibf_valid_i[6])inValid = 8'b0000_0011;
  else if(icache_ibf_valid_i[7])inValid = 8'b0000_0001;
  else                          inValid = 8'b0000_0000;
end

genvar i;
generate
  for(i=0;i<DEPTH;i=i+1)begin : Q
    always@(posedge clk or negedge rst_n)
      if(~rst_n) InstrQueue[i] <= 'd0;
      else if(notfull)begin
        if(tailPtAdd0 == i) InstrQueue[i] <= inValid[0] ? input2DM_OUT[0] : InstrQueue[i];
        if(tailPtAdd1 == i) InstrQueue[i] <= inValid[1] ? input2DM_OUT[1] : InstrQueue[i];
        if(tailPtAdd2 == i) InstrQueue[i] <= inValid[2] ? input2DM_OUT[2] : InstrQueue[i];
        if(tailPtAdd3 == i) InstrQueue[i] <= inValid[3] ? input2DM_OUT[3] : InstrQueue[i];
        if(tailPtAdd4 == i) InstrQueue[i] <= inValid[4] ? input2DM_OUT[4] : InstrQueue[i];
        if(tailPtAdd5 == i) InstrQueue[i] <= inValid[5] ? input2DM_OUT[5] : InstrQueue[i];
        if(tailPtAdd6 == i) InstrQueue[i] <= inValid[6] ? input2DM_OUT[6] : InstrQueue[i];
        if(tailPtAdd7 == i) InstrQueue[i] <= inValid[7] ? input2DM_OUT[7] : InstrQueue[i];
      end
  end
endgenerate
 
assign ibf_idu_instIn0_o = InstrQueue[headPtAdd0];
assign ibf_idu_instIn1_o = InstrQueue[headPtAdd1];
assign ibf_idu_instIn2_o = InstrQueue[headPtAdd2];
assign ibf_idu_instIn3_o = InstrQueue[headPtAdd3];
assign ibf_idu_instIn4_o = InstrQueue[headPtAdd4];
assign ibf_idu_instIn5_o = InstrQueue[headPtAdd5];
assign ibf_idu_instIn6_o = InstrQueue[headPtAdd6];
assign ibf_idu_instIn7_o = InstrQueue[headPtAdd7];

assign ibf_idu_valid_o[0] = MemUseNum >= 'd1;
assign ibf_idu_valid_o[1] = MemUseNum >= 'd2;
assign ibf_idu_valid_o[2] = MemUseNum >= 'd3;
assign ibf_idu_valid_o[3] = MemUseNum >= 'd4;
assign ibf_idu_valid_o[4] = MemUseNum >= 'd5;
assign ibf_idu_valid_o[5] = MemUseNum >= 'd6;
assign ibf_idu_valid_o[6] = MemUseNum >= 'd7;
assign ibf_idu_valid_o[7] = MemUseNum >= 'd8;


wire [15:0] Debug0input2DM_IN = input2DM_IN[0];
wire [15:0] Debug1input2DM_IN = input2DM_IN[1];
wire [15:0] Debug2input2DM_IN = input2DM_IN[2];
wire [15:0] Debug3input2DM_IN = input2DM_IN[3];
wire [15:0] Debug4input2DM_IN = input2DM_IN[4];
wire [15:0] Debug5input2DM_IN = input2DM_IN[5];
wire [15:0] Debug6input2DM_IN = input2DM_IN[6];
wire [15:0] Debug7input2DM_IN = input2DM_IN[7];

wire [15:0] Debug0input2DM_OUT = input2DM_OUT[0];
wire [15:0] Debug1input2DM_OUT = input2DM_OUT[1];
wire [15:0] Debug2input2DM_OUT = input2DM_OUT[2];
wire [15:0] Debug3input2DM_OUT = input2DM_OUT[3];
wire [15:0] Debug4input2DM_OUT = input2DM_OUT[4];
wire [15:0] Debug5input2DM_OUT = input2DM_OUT[5];
wire [15:0] Debug6input2DM_OUT = input2DM_OUT[6];
wire [15:0] Debug7input2DM_OUT = input2DM_OUT[7];

wire [15:0] Debug0InstrQueue = InstrQueue[0]; 
wire [15:0] Debug1InstrQueue = InstrQueue[1]; 
wire [15:0] Debug2InstrQueue = InstrQueue[2]; 
wire [15:0] Debug3InstrQueue = InstrQueue[3]; 
wire [15:0] Debug4InstrQueue = InstrQueue[4]; 
wire [15:0] Debug5InstrQueue = InstrQueue[5]; 
wire [15:0] Debug6InstrQueue = InstrQueue[6]; 
wire [15:0] Debug7InstrQueue = InstrQueue[7]; 
wire [15:0] Debug8InstrQueue = InstrQueue[8]; 
wire [15:0] Debug9InstrQueue = InstrQueue[9]; 
wire [15:0] Debug10InstrQueue = InstrQueue[10]; 
wire [15:0] Debug11InstrQueue = InstrQueue[11]; 
wire [15:0] Debug12InstrQueue = InstrQueue[12]; 
wire [15:0] Debug13InstrQueue = InstrQueue[13]; 
wire [15:0] Debug14InstrQueue = InstrQueue[14]; 
wire [15:0] Debug15InstrQueue = InstrQueue[15]; 
wire [15:0] Debug16InstrQueue = InstrQueue[16]; 
wire [15:0] Debug17InstrQueue = InstrQueue[17]; 
wire [15:0] Debug18InstrQueue = InstrQueue[18]; 
wire [15:0] Debug19InstrQueue = InstrQueue[19]; 
wire [15:0] Debug20InstrQueue = InstrQueue[20]; 
wire [15:0] Debug21InstrQueue = InstrQueue[21]; 
wire [15:0] Debug22InstrQueue = InstrQueue[22]; 
wire [15:0] Debug23InstrQueue = InstrQueue[23]; 
wire [15:0] Debug24InstrQueue = InstrQueue[24]; 
wire [15:0] Debug25InstrQueue = InstrQueue[25]; 
wire [15:0] Debug26InstrQueue = InstrQueue[26]; 
wire [15:0] Debug27InstrQueue = InstrQueue[27]; 
wire [15:0] Debug28InstrQueue = InstrQueue[28]; 
wire [15:0] Debug29InstrQueue = InstrQueue[29]; 
wire [15:0] Debug30InstrQueue = InstrQueue[30]; 
wire [15:0] Debug31InstrQueue = InstrQueue[31]; 
endmodule